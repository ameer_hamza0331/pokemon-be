const { Schema, model } = require("mongoose");

const pokemonSchema = new Schema({
  pokemonId: {
    type: String,
  },

  name: {
    type: String,
  },

  // description: {
  //   type: String,
  //   required: true,
  // },

  type: {
    type: String,
    required: true,
    enum: [
      "fire",
      "grass",
      "electric",
      "water",
      "ground",
      "fairy",
      "poison",
      "bug",
      "normal",
    ],
  },

  images: {
    type: String,
  },

  weight: {
    type: Number,
  },
  size: {
    type: Number,
  },
});

module.exports = model("pokemon", pokemonSchema);
