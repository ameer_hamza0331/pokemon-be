const { Schema, model } = require("mongoose");

const UserSchema = new Schema({
  first_name: {
    type: String,
    required: true,
  },

  last_name: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
  },

  password: {
    type: String,
    required: true,
  },
  capturedPokemon: {
    type: [Schema.Types.ObjectId],
    ref: "pokemon",
  },
});

module.exports = model("user", UserSchema);
