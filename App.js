const express = require("express");
const app = express();
let cors = require("cors");
const routes = require("./routers/routes");
const connectDB = require("./config/dataBase");

/*******AdminBro Dependencies */
const AdminJS = require("adminjs");
const buildAdminRouter = require("./adminbro/admin.routers");
const options = require("./adminbro/admin.options");
const admin = new AdminJS(options);

const router = buildAdminRouter(admin);
app.use(admin.options.rootPath, router);

app.use(express.json({ urlencoded: true }));
app.use(cors());

connectDB();
const PORT = process.env.PORT || 3700;
app.use("/", routes);
app.listen(PORT, () => {
  console.log(
    "The project is running on PORT 3700 AdminJS is under http://localhost:3700/admin"
  );
});
