const jwt = require("jsonwebtoken");

class TokenClass {
  issue(payload, expiresIn) {
    try {
      return jwt.sign(payload, "hytdnnjkicompanyjfjfdgjfgjfgffgt", {
        expiresIn: expiresIn ? expiresIn : "1d",
      });
    } catch (error) {
      return false;
    }
  }

  verify(token) {
    try {
      return jwt.verify(token, "hytdnnjkicompanyjfjfdgjfgjfgffgt");
    } catch (error) {
      return false;
    }
  }
}

module.exports = new TokenClass();
