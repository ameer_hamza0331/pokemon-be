const messageUtil = require("../utilities/message");
const {StatusCodes, getReasonPhrase} = require("http-status-codes");

class Response{
    
    ExistallReadyResponse = async(res, message) => {
        res.status(StatusCodes.CONFLICT, messageUtil.allReadyExist).send({
            success: true,
            message,
        });
    };


    successResponse = (res, message, data, token) => {
      const response = {
        success: true,
        message,
      };
    
      if (data) {
        response.data = data;
        response.token = token;
      }
    
      res.status(StatusCodes.OK).send(response);
    };

    authorizationErrorResponse = (res, message) => {
      res.status(StatusCodes.UNAUTHORIZED).send({
        success: false,
        message,
      });
    };

    serverErrorResponse = (res, error) => {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
          success: false,
          error: error.toString(),
          message: messageUtil.serverError,
        });
      };

      notFoundResponse = (res, message) => {
        res.status(StatusCodes.NOT_FOUND).send({
          success: false,
          message,
        });
      };
}
module.exports = new Response();