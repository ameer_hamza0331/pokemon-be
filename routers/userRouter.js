var express = require("express");
var router = express.Router();
const userController = require("../controllers/userController");
const { checkToken } = require("../utilities/tokenAuth");

router.post("/signup", userController.SignUp);
router.post("/login", userController.Login);
router.post("/capturePokemon", checkToken, userController.CapturePokemon);
router.post("/uncapturePokemon", checkToken, userController.UncapturePokemon);

router.get("/auth", checkToken, userController.UserAuth);

module.exports = router;
