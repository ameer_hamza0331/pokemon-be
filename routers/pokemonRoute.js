var express = require("express");
var router = express.Router();
const pokemonController = require("../controllers/pokemonController");

router.get("/getAllPokemon", pokemonController.GetAllPokemon);
router.get("/getPokemonById/:id", pokemonController.GetPokemonById);
router.get("/addPokemons", pokemonController.AddPokemons);

module.exports = router;
