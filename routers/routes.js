const express = require("express");
var router = express.Router();
const userRouter = require("./userRouter");
const pokemonRouter = require("./pokemonRoute");

router.use("/user", userRouter);
router.use("/pokemon", pokemonRouter);

module.exports = router;
