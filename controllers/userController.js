const userService = require("../services/userService");
const { bcryptHash, comparePassword } = require("../utilities/password");
const jwtHelper = require("../utilities/jwt");
const {
  successResponse,
  authorizationErrorResponse,
  ExistallReadyResponse,
  serverErrorResponse,
  notFoundResponse,
} = require("../utilities/response");
const messageUtil = require("../utilities/message");
const res = require("express/lib/response");
const { allReadyExist } = require("../utilities/message");
class UserController {
  SignUp = async (req, res) => {
    const { first_name, last_name, email, password } = req.body;
    let error = [];

    if (!first_name) {
      error.push("First Name");
    }

    if (!last_name) {
      error.push("Last Name");
    }

    if (!email) {
      error.push("Email");
    }

    if (!password) {
      error.push("Password");
    }

    if (error.length > 0) {
      error.join(", ");
      res.send({
        message: `Please Enter ${error}`,
        status: "404",
      });
    }

    try {
      let user = await userService.findUser({ email });

      if (user) {
        return ExistallReadyResponse(res, messageUtil.allReadyExist);
      }
      user = await userService.createUser({ ...req.body });
      user.password = await bcryptHash(password);
      user.save();
      const token = await jwtHelper.issue({ _id: user._id });
      return successResponse(res, messageUtil.ok, user, token);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  Login = async (req, res) => {
    const { email, password } = req.body;

    let error = [];
    if (!email) {
      error.push("email");
    }
    if (!password) {
      error.push("Password");
    }

    if (error.length > 0) {
      error.join(", ");

      return res.send({
        message: `please Enter ${error}`,
        status: "404",
      });
    }

    try {
      let user = await userService.findUser({ email });

      if (!user) {
        return notFoundResponse(res, messageUtil.notFound);
      }
      const isMatch = await comparePassword(password, user.password);
      if (!isMatch) {
        return authorizationErrorResponse(res, messageUtil.incorrectPassword);
      }
      const token = await jwtHelper.issue({ _id: user._id });

      return successResponse(res, messageUtil.ok, user, token);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  CapturePokemon = async (req, res) => {
    const { capturedPokemon } = req.body;

    if (!capturedPokemon) {
      return res.send({
        message: "Please Select Pokemon",
        status: "400",
      });
    }
    try {
      let findUser = await userService.findUser({ _id: req.userId });

      if (!findUser) {
        return notFoundResponse(res, messageUtil.notFound);
      }

      findUser = await userService.AddPokemon(
        { _id: req.userId },
        { $push: { capturedPokemon: capturedPokemon } }
      );

      let user = await userService.findUser({ _id: req.userId });

      return successResponse(res, messageUtil.ok, user);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  UncapturePokemon = async (req, res) => {
    const { capturedPokemon } = req.body;

    if (!capturedPokemon) {
      return res.send({
        message: "Please Select Pokemon",
        status: "400",
      });
    }
    try {
      let findUser = await userService.findUser({ _id: req.userId });

      if (!findUser) {
        return notFoundResponse(res, messageUtil.notFound);
      }

      await userService.AddPokemon(
        { _id: req.userId },
        { $pull: { capturedPokemon: capturedPokemon } }
      );

      let user = await userService.findUser({ _id: req.userId });
      return successResponse(res, messageUtil.ok, user);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  UserAuth = async (req, res) => {
    let user;
    try {
      user = await userService.findUser({ _id: req.userId });
      if (!user) {
        return notFoundResponse(res, messageUtil.notFound);
      } else {
        return successResponse(res, messageUtil.ok, user);
      }
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };
}

module.exports = new UserController();
