const messageUtil = require("../utilities/message");
const pokemonService = require("../services/pokemonService");
const axios = require("axios");
const {
  successResponse,
  serverErrorResponse,
  notFoundResponse,
} = require("../utilities/response");
class pokemonController {
  GetAllPokemon = async (req, res) => {
    try {
      let pokemon = await pokemonService.findAll();
      if (pokemon.length == 0) {
        return notFoundResponse(res, messageUtil.notFound);
      }
      return successResponse(res, messageUtil.ok, pokemon);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  GetPokemonById = async (req, res) => {
    const { id } = req.params;

    if (!id) {
      return res.send({
        message: `Please Insert pokemon ID`,
        status: "400",
      });
    }
    try {
      let pokemon = await pokemonService.findPokemonById({ _id: id });
      if (!pokemon) {
        return notFoundResponse(res, messageUtil.notFound);
      }
      return successResponse(res, messageUtil.ok, pokemon);
    } catch (error) {
      return serverErrorResponse(res, error);
    }
  };

  AddPokemons = async (req, res) => {
    let URL = "https://pokeapi.co/api/v2/pokemon";
    for (let i = 1; i <= 10; i++) {
      const response = await axios.get(`${URL}/${i}`);

      let pokemonData = {
        pokemonId: response.data.id,
        name: response.data.name,
        // description: response.data,
        type: response.data.types[0]?.type?.name,
        images: response.data.sprites?.front_default,
        weight: response.data.weight,
        size: response.data.height,
      };

      await pokemonService.addPokemon({
        ...pokemonData,
      });
    }
    return successResponse(res, messageUtil.ok);
  };
}

module.exports = new pokemonController();
