const UserSchema = require("../models/user");

class UserCRUD {
  findUser = async (query) => {
    return await UserSchema.findOne(query)
      .populate("capturedPokemon")
      .select("-__v  ");
  };
  createUser = async (query) => {
    return await UserSchema.create(query);
  };

  AddPokemon = async (query, data) => {
    return await UserSchema.findOneAndUpdate(query, data, { new: true }).select(
      "-__v -password -createdAt -updatedAt"
    );
  };
}
module.exports = new UserCRUD();
