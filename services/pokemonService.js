const pokemonSchema = require("../models/pokemon");

class pokemonCRUD {
  findAll = async () => {
    return await pokemonSchema.find().select("-__v  ").sort({ _id: -1 });
  };
  findPokemonById = async (query) => {
    return await pokemonSchema.findOne(query).select("-__v  ");
  };
  addPokemon = async (query) => {
    console.log("query", query);

    let pokemon = await pokemonSchema.create(query);
    return pokemon;
  };
}

module.exports = new pokemonCRUD();
