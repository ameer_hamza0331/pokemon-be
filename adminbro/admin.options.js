const AdminJS = require("adminjs");
const AdminJSMongoose = require("@adminjs/mongoose");
const uploadFeature = require("@adminjs/upload");
AdminJS.registerAdapter(AdminJSMongoose);
require("dotenv").config();

const pokemon = require("../models/pokemon");
const user = require("../models/user");

const locale = {
  translations: {
    labels: {
      // change Heading for Login
      loginWelcome: "Pokemon",
    },
    messages: {
      loginWelcome: "Welcome to Pokemon Admin panel",
    },
  },
};

const options = {
  locale,
  resources: [
    user,
    {
      resource: pokemon,
      options: {
        listProperties: [
          "pokemonId",
          "name",
          "type",
          "images",
          "weight",
          "size",
        ],
      },

      features: [
        uploadFeature({
          provider: {
            aws: {
              // bucket: process.env.AWS_bucket,
              // secretAccessKey: process.env.AWS_secretkey,
              // accessKeyId: process.env.AWS_accessKeyId,
              bucket: "solaire-storage",

              secretAccessKey: "b8n0QkiJmdbf/q8b/2sOLD9CaWsorwRtMALqz+Jg",
              accessKeyId: "AKIAQPHEPZNHOXEEAF6E",
            },
          },
          properties: {
            key: "images",
            mimeType: "/jpeg|jpg|png|gif|pdf|bmp|jpg_small/",
            filePath: `filePath`,
          },
        }),
      ],
    },
  ],
  dashboard: {
    component: AdminJS.bundle("./myDashboardComponent"),
  },
  branding: {
    companyName: "Pokemon",
    softwareBrothers: false,
    logo: "",
  },
};

module.exports = options;
